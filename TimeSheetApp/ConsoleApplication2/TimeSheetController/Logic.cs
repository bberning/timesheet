﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheetController
{

    public class Logic
    {
        public static Double TotalWeekHours = 168;

        public Boolean ValidateHours(Double work, Double sick, Double vac)
        {
            return (work + sick + vac) <= TotalWeekHours && (work + sick + vac) >= 0 ;
        }

        public Boolean ValidateInfo(String input)
        {
            double num;
            return double.TryParse(input, out num) && Convert.ToDouble(input) >= 0;
        }
       
        public Double CalculatePay(Double week1hours,Double week2hours, Double pay)
        {
            return (week1hours+week2hours) * pay;
        }
        public String ConstructPayMessage(Double pay)
        {
            return String.Format("This is your total pay for two weeks: {0:C2}",pay);
        }
    }
}
