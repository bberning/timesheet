﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheetController
{
    public class Employee
    {
        private Double payrate;

        public Double Payrate
        {
            get { return payrate; }
            set { payrate = value; }
        }

    }
}
