﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheetController
{
    public class Week
    {
        private Double workhours;

        public Double WorkHours
        {
            get { return workhours; }
            set { workhours = value; }
        }

        private Double sickhours;

        public Double SickHours
        {
            get { return sickhours; }
            set { sickhours = value; }
        }


        private Double vachours;

        public Double VacHours
        {
            get { return vachours; }
            set { vachours = value; }
        }

        private Double overtime;

        public Double Overtime
        {
            get { return overtime; }
            
        }



        public void CalculateOvertime()
        {
            Double leftover = workhours - 40;
            if (leftover > 0)
            {
                workhours -= leftover;
                overtime += leftover + (leftover * .5);

            }
        }



        public Double CalculateTotalHours()
        {
            return workhours + sickhours + vachours + overtime;
        }
    }
}
