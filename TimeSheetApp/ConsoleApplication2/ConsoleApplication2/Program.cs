﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheetController;

namespace ConsoleApplication2
{
    public class Program
    {

        public static Logic l = new Logic();
        static void Main(string[] args)
        {
            Program p = new Program();
            p.Run();

        }

        public void Run()
        {

            Week weekone = new Week();
            Week weektwo = new Week();
            Employee e = new Employee();

            e.Payrate = Math.Round(Getinfo("Enter your payrate."), 2);

            getHours("Enter the following information for the first week.", weekone);
            getHours("Enter the following information for the second week.", weektwo);

            weekone.CalculateOvertime();
            weektwo.CalculateOvertime();

            Console.WriteLine("WorkHours: {0}", (weekone.WorkHours + weektwo.WorkHours));
            Console.WriteLine("SickHours: {0}", (weekone.SickHours + weektwo.SickHours));
            Console.WriteLine("VacationHours: {0}", (weekone.VacHours + weektwo.VacHours));
            Console.WriteLine("OvertimeHours: {0}", (weekone.Overtime + weektwo.Overtime));

            Console.WriteLine("Your total pay is {0:C2}", l.CalculatePay( weekone.CalculateTotalHours(), weektwo.CalculateTotalHours(), e.Payrate));
        }
        public Double Getinfo(String msg)
        {
            Boolean badvalue = true;
            Double output = 0;
            while (badvalue)
            {
                Console.WriteLine("{0}", msg);
                String input = Console.ReadLine();
                if (l.ValidateInfo(input))
                {
                    output = Convert.ToDouble(input);
                    badvalue = false;
                }
                else
                {
                    Console.WriteLine("That was not a valid input.");
                }
            }
            return output;
        }
        
        public void getHours(String msg, Week week)
        {
            Console.WriteLine(msg);
            Boolean badvalue = true;

            Double workhours = 0;
            Double sickhours = 0;
            Double vacationhours = 0;


            while (badvalue)
            {
                Console.WriteLine("Enter work hours.");
                String workinput = Console.ReadLine();

                Console.WriteLine("Enter sick hours.");
                String sickinput = Console.ReadLine();

                Console.WriteLine("Enter vacation hours.");
                String vacinput = Console.ReadLine();

                if(l.ValidateInfo(workinput) && l.ValidateInfo(sickinput) && l.ValidateInfo(vacinput))
                {
                    workhours = Convert.ToDouble(workinput);
                    sickhours = Convert.ToDouble(sickinput);
                    vacationhours = Convert.ToDouble(vacinput);

                    if (l.ValidateHours(workhours, sickhours, vacationhours))
                    {
                        badvalue = false;

                    }
                    else
                    {
                        Console.WriteLine("The total hours entered was greater than the number of allowed hours for one week.Please enter them again.");
                    }
                }
                else
                {
                    Console.WriteLine("Those were not valid inputs.Please enter them again.");
                }

            }
            week.WorkHours = workhours;
            week.SickHours = sickhours;
            week.VacHours = vacationhours;
        }

     

    }
}
