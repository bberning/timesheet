﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TimeSheetController;

namespace TimesheetWindowsForm
{
    public partial class Form1 : Form
    {
        private Logic l = new Logic();

        public Form1()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
        private void calculateButton_Click(object sender, EventArgs e)
        {
            Week weekone = new Week();
            Week weektwo = new Week();
            Employee em = new Employee();
            Boolean passed = true;
            string error = "";


            passed = (CheckWeekData(this.week1HoursBox, this.week1SickBox, this.week1VacBox, weekone) && CheckWeekData(this.week2HoursBox, this.week2SickBox, this.week2VacBox, weektwo) && CheckPayrate(this.payrateBox, em)) ;

      
            if (!passed)
            {
                this.label1.ForeColor = System.Drawing.Color.Red;
                this.label1.Text = "Error! Please input correct numbers only. The text in red needs correct input";
            }
            else if (!l.ValidateHours(weekone.WorkHours, weekone.SickHours, weekone.VacHours) || !l.ValidateHours(weektwo.WorkHours, weektwo.SickHours, weektwo.VacHours))
            {
                if (!l.ValidateHours(weekone.WorkHours, weekone.SickHours, weekone.VacHours))
                {
                    error += "week1";
                }
                if(!l.ValidateHours(weektwo.WorkHours, weektwo.SickHours, weektwo.VacHours))
                {
                    if(error.Length != 0)
                    {
                        error += ", week2";
                    }
                    else
                    {
                        error += "week2";
                    }
                    
                }
                this.label1.ForeColor = System.Drawing.Color.Red;
                this.label1.Text = "Error! Your weekly hours are too high or too low ( "+error+
                    " ). Please enter correct info.";
            }
            else
            {
                weekone.CalculateOvertime();
                weektwo.CalculateOvertime();
                double wk1pay = weekone.CalculateTotalHours();
                double wk2pay = weektwo.CalculateTotalHours();
                double payment = l.CalculatePay(wk1pay, wk2pay, em.Payrate);
                this.label1.ForeColor = System.Drawing.Color.Green;
                String work = "Work Hours: " + (weekone.WorkHours + weektwo.WorkHours) + "\n" ;
                String sick = "Sick Hours: " + (weekone.SickHours + weektwo.SickHours) + "\n";
                String vac = "Vacation Hours: " + (weekone.VacHours + weektwo.VacHours) + "\n";
                String overtime = "Overtime Hours: " + (weekone.Overtime + weektwo.Overtime) + "\n";

                this.label1.Text =  work + sick + vac + overtime + l.ConstructPayMessage(payment);
            }
        }

        public Boolean CheckWeekData(TextBox work, TextBox sick, TextBox vac, Week week)
        {
            Boolean goodvalues = true;
            if (l.ValidateInfo(work.Text))
            {
                week.WorkHours = Convert.ToDouble(work.Text);
                work.ForeColor = Color.Black;
            }
            else
            {
                goodvalues = false;
                work.ForeColor = Color.Red;
            }
            if (l.ValidateInfo(sick.Text))
            {
                week.SickHours = Convert.ToDouble(sick.Text);
                sick.ForeColor = Color.Black;
            }
            else
            {
                goodvalues = false;
                sick.ForeColor = Color.Red;
            }
            if (l.ValidateInfo(vac.Text))
            {
                week.VacHours = Convert.ToDouble(vac.Text);
                vac.ForeColor = Color.Black;
            }
            else
            {
                goodvalues = false;
                vac.ForeColor = Color.Red;
            }

            return goodvalues;
        }

        public Boolean CheckPayrate(TextBox payrate, Employee e)
        {
            Boolean goodvalue = true;
            if (l.ValidateInfo(payrate.Text))
            {
                e.Payrate = Convert.ToDouble(payrate.Text);
                payrate.ForeColor = Color.Black;
            }
            else
            {
                goodvalue = false;
                payrate.ForeColor = Color.Red;
            }
            return goodvalue;
        }
    }
}
