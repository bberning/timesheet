﻿namespace TimesheetWindowsForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.payrateBox = new System.Windows.Forms.TextBox();
            this.hourlyPayrate = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.week1VacBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.week2VacBox = new System.Windows.Forms.TextBox();
            this.week1SickBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.week1Vac = new System.Windows.Forms.Label();
            this.week2SickBox = new System.Windows.Forms.TextBox();
            this.week1Hours = new System.Windows.Forms.Label();
            this.week2HoursBox = new System.Windows.Forms.TextBox();
            this.week1Sick = new System.Windows.Forms.Label();
            this.week1HoursBox = new System.Windows.Forms.TextBox();
            this.week2Vac = new System.Windows.Forms.Label();
            this.week2Hours = new System.Windows.Forms.Label();
            this.week2Sick = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.payrateBox);
            this.panel1.Controls.Add(this.hourlyPayrate);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(-1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(552, 410);
            this.panel1.TabIndex = 0;
            // 
            // payrateBox
            // 
            this.payrateBox.Location = new System.Drawing.Point(217, 38);
            this.payrateBox.Name = "payrateBox";
            this.payrateBox.Size = new System.Drawing.Size(100, 20);
            this.payrateBox.TabIndex = 15;
            // 
            // hourlyPayrate
            // 
            this.hourlyPayrate.AutoSize = true;
            this.hourlyPayrate.Location = new System.Drawing.Point(228, 22);
            this.hourlyPayrate.Name = "hourlyPayrate";
            this.hourlyPayrate.Size = new System.Drawing.Size(79, 13);
            this.hourlyPayrate.TabIndex = 0;
            this.hourlyPayrate.Text = "Hourly Payrate:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.week1VacBox);
            this.panel2.Controls.Add(this.calculateButton);
            this.panel2.Controls.Add(this.week2VacBox);
            this.panel2.Controls.Add(this.week1SickBox);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.week1Vac);
            this.panel2.Controls.Add(this.week2SickBox);
            this.panel2.Controls.Add(this.week1Hours);
            this.panel2.Controls.Add(this.week2HoursBox);
            this.panel2.Controls.Add(this.week1Sick);
            this.panel2.Controls.Add(this.week1HoursBox);
            this.panel2.Controls.Add(this.week2Vac);
            this.panel2.Controls.Add(this.week2Hours);
            this.panel2.Controls.Add(this.week2Sick);
            this.panel2.Location = new System.Drawing.Point(4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(545, 403);
            this.panel2.TabIndex = 22;
            // 
            // week1VacBox
            // 
            this.week1VacBox.Location = new System.Drawing.Point(48, 142);
            this.week1VacBox.Name = "week1VacBox";
            this.week1VacBox.Size = new System.Drawing.Size(100, 20);
            this.week1VacBox.TabIndex = 18;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(224, 142);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 14;
            this.calculateButton.Text = "Calculate";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // week2VacBox
            // 
            this.week2VacBox.Location = new System.Drawing.Point(365, 142);
            this.week2VacBox.Name = "week2VacBox";
            this.week2VacBox.Size = new System.Drawing.Size(100, 20);
            this.week2VacBox.TabIndex = 21;
            // 
            // week1SickBox
            // 
            this.week1SickBox.Location = new System.Drawing.Point(48, 85);
            this.week1SickBox.Name = "week1SickBox";
            this.week1SickBox.Size = new System.Drawing.Size(100, 20);
            this.week1SickBox.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 207);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 23);
            this.label1.TabIndex = 0;
            this.label1.AutoSize = true;
            this.label1.MaximumSize = new System.Drawing.Size(450,0);
            this.label1.Text = "";
            // 
            // week1Vac
            // 
            this.week1Vac.AutoSize = true;
            this.week1Vac.Location = new System.Drawing.Point(37, 126);
            this.week1Vac.Name = "week1Vac";
            this.week1Vac.Size = new System.Drawing.Size(124, 13);
            this.week1Vac.TabIndex = 3;
            this.week1Vac.Text = "Week 1 Vacation Hours:";
            // 
            // week2SickBox
            // 
            this.week2SickBox.Location = new System.Drawing.Point(365, 85);
            this.week2SickBox.Name = "week2SickBox";
            this.week2SickBox.Size = new System.Drawing.Size(100, 20);
            this.week2SickBox.TabIndex = 20;
            // 
            // week1Hours
            // 
            this.week1Hours.AutoSize = true;
            this.week1Hours.Location = new System.Drawing.Point(61, 18);
            this.week1Hours.Name = "week1Hours";
            this.week1Hours.Size = new System.Drawing.Size(77, 13);
            this.week1Hours.TabIndex = 1;
            this.week1Hours.Text = "Week 1 hours:";
            // 
            // week2HoursBox
            // 
            this.week2HoursBox.Location = new System.Drawing.Point(365, 34);
            this.week2HoursBox.Name = "week2HoursBox";
            this.week2HoursBox.Size = new System.Drawing.Size(100, 20);
            this.week2HoursBox.TabIndex = 19;
            // 
            // week1Sick
            // 
            this.week1Sick.AutoSize = true;
            this.week1Sick.Location = new System.Drawing.Point(48, 69);
            this.week1Sick.Name = "week1Sick";
            this.week1Sick.Size = new System.Drawing.Size(103, 13);
            this.week1Sick.TabIndex = 2;
            this.week1Sick.Text = "Week 1 Sick Hours:";
            // 
            // week1HoursBox
            // 
            this.week1HoursBox.Location = new System.Drawing.Point(48, 34);
            this.week1HoursBox.Name = "week1HoursBox";
            this.week1HoursBox.Size = new System.Drawing.Size(100, 20);
            this.week1HoursBox.TabIndex = 16;
            // 
            // week2Vac
            // 
            this.week2Vac.AutoSize = true;
            this.week2Vac.Location = new System.Drawing.Point(354, 126);
            this.week2Vac.Name = "week2Vac";
            this.week2Vac.Size = new System.Drawing.Size(124, 13);
            this.week2Vac.TabIndex = 6;
            this.week2Vac.Text = "Week 2 Vacation Hours:";
            // 
            // week2Hours
            // 
            this.week2Hours.AutoSize = true;
            this.week2Hours.Location = new System.Drawing.Point(374, 18);
            this.week2Hours.Name = "week2Hours";
            this.week2Hours.Size = new System.Drawing.Size(79, 13);
            this.week2Hours.TabIndex = 4;
            this.week2Hours.Text = "Week 2 Hours:";
            // 
            // week2Sick
            // 
            this.week2Sick.AutoSize = true;
            this.week2Sick.Location = new System.Drawing.Point(364, 69);
            this.week2Sick.Name = "week2Sick";
            this.week2Sick.Size = new System.Drawing.Size(103, 13);
            this.week2Sick.TabIndex = 5;
            this.week2Sick.Text = "Week 2 Sick Hours:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(550, 408);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Timesheet Form";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label week2Vac;
        private System.Windows.Forms.Label week2Sick;
        private System.Windows.Forms.Label week2Hours;
        private System.Windows.Forms.Label week1Vac;
        private System.Windows.Forms.Label week1Sick;
        private System.Windows.Forms.Label week1Hours;
        private System.Windows.Forms.Label hourlyPayrate;
        private System.Windows.Forms.TextBox week2VacBox;
        private System.Windows.Forms.TextBox week2SickBox;
        private System.Windows.Forms.TextBox week2HoursBox;
        private System.Windows.Forms.TextBox week1VacBox;
        private System.Windows.Forms.TextBox week1SickBox;
        private System.Windows.Forms.TextBox week1HoursBox;
        private System.Windows.Forms.TextBox payrateBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
    }
}

