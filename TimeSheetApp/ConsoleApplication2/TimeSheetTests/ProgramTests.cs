﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplication2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheetController;
namespace ConsoleApplication2.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        public static Double margin = .00001;

        [TestMethod()]
        public void CalculatePayTest()
        {
            Logic p = new Logic();
            double expected = 18;
            double actual = p.CalculatePay(3,0, 6);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CalculatePayTestWithNull()
        {
            Logic p = new Logic();
            double expected = 0;
            double actual = p.CalculatePay(0,0, 6);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CalculatePayTestWithDouble()
        {
            Logic p = new Logic();
            double expected = 37.17;
            double actual = p.CalculatePay(4.9,1, 6.3);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]

        public void CalculateOvertime()
        {
            Week w = new Week();
            w.WorkHours = 45;
            w.CalculateOvertime();
            double expected = 7.5;
            double actual = w.Overtime;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]

        public void CalculateOvertimeWithNull()
        {
            Week w = new Week();
            w.WorkHours = 0;
            w.CalculateOvertime();
            double expected = 0;
            double actual = w.Overtime;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]

        public void CalculateOvertimeWithdecimal()
        {
            Week w = new Week();
            w.WorkHours = 40.6;
            w.CalculateOvertime();
            Boolean  expected = true;
            double expectedanswer = 40.9;
            double actualanswer = w.Overtime;
            double difference = Math.Abs(actualanswer * margin);
            Boolean actual = (actualanswer - expectedanswer) <= difference;
            
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]

        public void CalculateOvertimeWithnegative()
        {
            Week w = new Week();
            w.WorkHours = -1;
            w.CalculateOvertime();
            double expected = 0;
            double actual = w.Overtime;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]

        public void CalculateOvertimeWithupperlimit()
        {
            Week w = new Week();
            w.WorkHours = 40;
            w.CalculateOvertime();
            double expected = 0;
            double actual = w.Overtime;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]

        public void Validateinputpassing()
        {
            Logic p = new Logic();
            Boolean expected = true;
            Boolean actual = p.ValidateInfo("40");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Validateinputfailing()
        {
            Logic p = new Logic();
            Boolean expected = false;
            Boolean actual = p.ValidateInfo("abc");
            Assert.AreEqual(expected, actual);
        }

    

    [TestMethod()]
    public void ValidateHoursPassing()
    {
        Logic p = new Logic();
        Boolean expected = true;
        Boolean actual = p.ValidateHours(40,10,10);
        Assert.AreEqual(expected, actual);
    }


        [TestMethod()]
        public void ValidateHoursFailing()
        {
            Logic p = new Logic();
            Boolean expected = false;
            Boolean actual = p.ValidateHours(40, 70, 70);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void CalculateHours()
        {
            Week w = new Week();
            w.WorkHours = 40;
            w.SickHours = 30;
            w.VacHours = 30;
            Double expected = 100;
            Double actual = w.CalculateTotalHours();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CalculateHoursNull()
        {
            Week w = new Week();
            w.WorkHours = 0;
            w.SickHours = 0;
            w.VacHours = 0;
            Double expected = 0;
            Double actual = w.CalculateTotalHours();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CalculateHoursNegative()
        {
            Week w = new Week();
            w.WorkHours = 20;
            w.SickHours = -30;
            w.VacHours = 0;
            Double expected = -10;
            Double actual = w.CalculateTotalHours();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CalculateHoursFraction()
        {
            Week w = new Week();
            w.WorkHours = 40;
            w.SickHours = 10;
            w.VacHours = .5;
            Boolean expected = true;
            Double expectedanswer = 50.5;
            Double actualanswer = w.CalculateTotalHours();
            double difference = Math.Abs(actualanswer * margin);
            Boolean actual = (actualanswer - expectedanswer) <= difference;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ConstructPayMessage()
        {
            Logic p = new Logic();
            String expected = "This is your total pay for two weeks: $1,000.00";
            Boolean ignorecase = true;
            String actual = p.ConstructPayMessage(1000);
            Assert.AreEqual(expected, actual, ignorecase);
        }
        [TestMethod()]
        public void EmployeePayRate()
        {
            Employee e = new Employee();
            e.Payrate = 10.05;
            double expected = 10.05;
            Assert.AreEqual(expected, e.Payrate);
        }
        [TestMethod()]
        public void WeekGetSickHours()
        {
            Week w = new Week();
            w.WorkHours = 5;
            double expected = 5;
            Assert.AreEqual(expected, w.WorkHours);
        }
        [TestMethod()]
        public void WeekGetVacHours()
        {
            Week w = new Week();
            w.VacHours = 5;
            double expected = 5;
            Assert.AreEqual(expected, w.VacHours);
        }
        [TestMethod()]
        public void WeekGetWorkHours()
        {
            Week w = new Week();
            w.SickHours = 5;
            double expected = 5;
            Assert.AreEqual(expected, w.SickHours);
        }
    }

}